#ifndef __CONTROLCONFIGCLIENT_H__
#define __CONTROLCONFIGCLIENT_H__

#include <string>
#include <future>
#include <functional>
#include <exception>

#include "Frame.pb.h"
#include "ControlConfig.pb.h"

#include "Frame.h"
#include "IRouterClient.h"
#include "NotificationHandler.h"

namespace Kinova
{
namespace Api
{
	namespace ControlConfig
	{
		// todogr move somewhere else
		const std::string   none = "";
		
		enum FunctionUids
		{
			eUidSetGravityVector = 0x100001,
			eUidGetGravityVector = 0x100002,
			eUidSetPayloadInformation = 0x100003,
			eUidGetPayloadInformation = 0x100004,
			eUidSetToolConfiguration = 0x100005,
			eUidGetToolConfiguration = 0x100006,
			eUidControlConfigurationTopic = 0x100007,
			eUidUnsubscribe = 0x100008,
			eUidSetCartesianReferenceFrame = 0x100009,
			eUidGetCartesianReferenceFrame = 0x10000a,
		};
		
		class ControlConfigClient
		{
			static const uint32_t m_serviceVersion = 1;
			static const uint32_t m_serviceId = eIdControlConfig;
			NotificationHandler m_notificationHandler;

		protected:
			IRouterClient* const m_clientRouter;

		public:
			ControlConfigClient(IRouterClient* clientRouter);
			static uint32_t getUniqueFctId(uint16_t fctId);

			void SetGravityVector(const GravityVector& gravityvector, uint32_t deviceId = 0, const RouterClientSendOptions& options = {false, 0, 3000});
			void SetGravityVector_callback(const GravityVector& gravityvector, std::function< void (const Error&) > callback, uint32_t deviceId = 0);
			std::future<void> SetGravityVector_async(const GravityVector& gravityvector, uint32_t deviceId = 0, const RouterClientSendOptions& options = {false, 0, 3000});

			GravityVector GetGravityVector(uint32_t deviceId = 0, const RouterClientSendOptions& options = {false, 0, 3000});
			void GetGravityVector_callback(std::function< void (const Error&, const GravityVector&) > callback, uint32_t deviceId = 0);
			std::future<GravityVector> GetGravityVector_async(uint32_t deviceId = 0, const RouterClientSendOptions& options = {false, 0, 3000});

			void SetPayloadInformation(const PayloadInformation& payloadinformation, uint32_t deviceId = 0, const RouterClientSendOptions& options = {false, 0, 3000});
			void SetPayloadInformation_callback(const PayloadInformation& payloadinformation, std::function< void (const Error&) > callback, uint32_t deviceId = 0);
			std::future<void> SetPayloadInformation_async(const PayloadInformation& payloadinformation, uint32_t deviceId = 0, const RouterClientSendOptions& options = {false, 0, 3000});

			PayloadInformation GetPayloadInformation(uint32_t deviceId = 0, const RouterClientSendOptions& options = {false, 0, 3000});
			void GetPayloadInformation_callback(std::function< void (const Error&, const PayloadInformation&) > callback, uint32_t deviceId = 0);
			std::future<PayloadInformation> GetPayloadInformation_async(uint32_t deviceId = 0, const RouterClientSendOptions& options = {false, 0, 3000});

			void SetToolConfiguration(const ToolConfiguration& toolconfiguration, uint32_t deviceId = 0, const RouterClientSendOptions& options = {false, 0, 3000});
			void SetToolConfiguration_callback(const ToolConfiguration& toolconfiguration, std::function< void (const Error&) > callback, uint32_t deviceId = 0);
			std::future<void> SetToolConfiguration_async(const ToolConfiguration& toolconfiguration, uint32_t deviceId = 0, const RouterClientSendOptions& options = {false, 0, 3000});

			ToolConfiguration GetToolConfiguration(uint32_t deviceId = 0, const RouterClientSendOptions& options = {false, 0, 3000});
			void GetToolConfiguration_callback(std::function< void (const Error&, const ToolConfiguration&) > callback, uint32_t deviceId = 0);
			std::future<ToolConfiguration> GetToolConfiguration_async(uint32_t deviceId = 0, const RouterClientSendOptions& options = {false, 0, 3000});

			Kinova::Api::Common::NotificationHandle OnNotificationControlConfigurationTopic(std::function< void (ControlConfigurationNotification) > callback, const Kinova::Api::Common::NotificationOptions& notificationoptions, uint32_t deviceId = 0, const RouterClientSendOptions& options = {false, 0, 3000});

			void Unsubscribe(const Kinova::Api::Common::NotificationHandle& notificationhandle, uint32_t deviceId = 0, const RouterClientSendOptions& options = {false, 0, 3000});

			void SetCartesianReferenceFrame(const CartesianReferenceFrameInfo& cartesianreferenceframeinfo, uint32_t deviceId = 0, const RouterClientSendOptions& options = {false, 0, 3000});
			void SetCartesianReferenceFrame_callback(const CartesianReferenceFrameInfo& cartesianreferenceframeinfo, std::function< void (const Error&) > callback, uint32_t deviceId = 0);
			std::future<void> SetCartesianReferenceFrame_async(const CartesianReferenceFrameInfo& cartesianreferenceframeinfo, uint32_t deviceId = 0, const RouterClientSendOptions& options = {false, 0, 3000});

			CartesianReferenceFrameInfo GetCartesianReferenceFrame(uint32_t deviceId = 0, const RouterClientSendOptions& options = {false, 0, 3000});
			void GetCartesianReferenceFrame_callback(std::function< void (const Error&, const CartesianReferenceFrameInfo&) > callback, uint32_t deviceId = 0);
			std::future<CartesianReferenceFrameInfo> GetCartesianReferenceFrame_async(uint32_t deviceId = 0, const RouterClientSendOptions& options = {false, 0, 3000});


		private:
			void messageHeaderValidation(const Frame& msgFrame){ /* todogr ... */ }
		};
	}
}
}

#endif