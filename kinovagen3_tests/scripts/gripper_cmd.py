#!/usr/bin/env python

import rospy
# Brings in the SimpleActionClient
import actionlib
from control_msgs.msg import GripperCommandAction, GripperCommandGoal, GripperCommandFeedback

class GripperClient(object):

    def __init__(self):
        # Creates the SimpleActionClient, passing the type of the action

        # We create some constants with the corresponing vaules from the SimpleGoalState class
        self.PENDING = 0
        self.ACTIVE = 1
        self.DONE = 2
        self.WARN = 3
        self.ERROR = 4

        self.client = actionlib.SimpleActionClient('/my_gen3/robotiq_2f_85_gripper_controller/gripper_cmd', GripperCommandAction)
        self.gripper_feedback = GripperCommandFeedback()
        # Waits until the action server has started up and started
        # listening for goals.
        rospy.logwarn("Waiting for server....")
        self.client.wait_for_server()
        rospy.logwarn("Waiting for server....DONE")

    def update_gripper(self, position, max_effort = 1.0, wait_for_result_time = 0.0):
        """
        Change gripper opening
        control_msgs/GripperCommandActionGoal
            std_msgs/Header header
                uint32 seq
                time stamp
                string frame_id
            actionlib_msgs/GoalID goal_id
                time stamp
                string id
            control_msgs/GripperCommandGoal goal
                control_msgs/GripperCommand command
                    float64 position
                    float64 max_effort
        """

        # Creates a goal to send to the action server.
        gripper_goal = GripperCommandGoal()
        gripper_goal.command.position = position
        gripper_goal.command.max_effort = max_effort

        # Sends the goal to the action server.
        self.client.send_goal(gripper_goal, feedback_cb=self.feedback_callback)

        # Waits for the server to finish performing the action.
        if wait_for_result_time == 0.0:
            self.client.wait_for_result()
        else:

            state_result = self.client.get_state()

            rate = rospy.Rate(10)
            init_time = rospy.get_time()
            now_time = rospy.get_time()
            delta_time = now_time - init_time
            rospy.logdebug("state_result: "+str(state_result))

            while state_result < self.DONE and delta_time < wait_for_result_time:
                rate.sleep()
                state_result = self.client.get_state()
                rospy.logdebug("state_result: "+str(state_result))
                now_time = rospy.get_time()
                delta_time = now_time - init_time
                rospy.logdebug("DeltaTime: "+str(delta_time))

            if delta_time >= wait_for_result_time:
                rospy.logerr("MAXXED Wait Time: "+str(delta_time))
            else:
                rospy.logdebug("Wait Time DONE: "+str(delta_time))

        # Prints out the result of executing the action
        return self.client.get_result()

    def feedback_callback(self, feedback):
        """
        We get the feedback
        control_msgs/GripperCommandFeedback
            float64 position
            float64 effort
            bool stalled
            bool reached_goal
        """
        self.gripper_feedback = feedback
        rospy.logwarn(str(self.gripper_feedback))


    def gripper_range_test(self, increments= 0.05, init_pos = 0.0):
        """
        We test the range of movemnt of the gripper
        """
        pos = init_pos
        rate_obj = rospy.Rate(10)
        gripper_range = [init_pos,None]

        for i in range(100):
            rospy.loginfo("Position="+str(pos))
            result = self.update_gripper(position=pos, wait_for_result_time=1.0)
            rospy.logdebug("RESULT="+str(result))
            if result is None:
                rospy.logerr("Position Couldn't be reached! MAX Position="+str(gripper_range[1]))
                break
            else:
                gripper_range[1] = pos
            pos += increments
            rate_obj.sleep()

        return gripper_range

    def gripper_cmd(self):

        rate = rospy.Rate(10)
        while not rospy.is_shutdown():
            value = float(raw_input("Value Gripper=="))
            result = self.update_gripper(position=value, wait_for_result_time=1.0)
            rate.sleep()

if __name__ == '__main__':
    try:
        # Initializes a rospy node so that the SimpleActionClient can
        # publish and subscribe over ROS.
        rospy.init_node('gripper_aclient', log_level=rospy.INFO)
        gripper_client_obj = GripperClient()
        #gripper_range = gripper_client_obj.gripper_range_test(increments=0.01)
        gripper_range = gripper_client_obj.gripper_cmd()
        rospy.logwarn("GripperRange="+str(gripper_range))
    except rospy.ROSInterruptException:
        rospy.logerr("program interrupted before completion")