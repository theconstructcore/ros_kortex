#!/usr/bin/env python

# Software License Agreement (BSD License)
#
# Copyright (c) 2013, SRI International
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above
#    copyright notice, this list of conditions and the following
#    disclaimer in the documentation and/or other materials provided
#    with the distribution.
#  * Neither the name of SRI International nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
# Author: Acorn Pooley, Mike Lautman

# Inspired from http://docs.ros.org/kinetic/api/moveit_tutorials/html/doc/move_group_python_interface/move_group_python_interface_tutorial.html
# Modified by Alexandre Vannobel to initialize simulated Kinova Gen3 robot in Gazebo
# Modified By MiguelAngelRodriguez from TheConstructSim to test moveit

import sys
import time
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
from std_srvs.srv import Empty

class TestMoveitKinova(object):
    def __init__(self):

        # Create the MoveItInterface necessary objects
        moveit_commander.roscpp_initialize(sys.argv)
        group_name = "arm"
        self.robot = moveit_commander.RobotCommander()
        self.scene = moveit_commander.PlanningSceneInterface()
        self.group = moveit_commander.MoveGroupCommander(group_name)

    def start_test(self):

        while not rospy.is_shutdown():
            # We execute all the movements loaded through ros_kortex/kortex_move_it_config/gen3_move_it_config/config/gen3.srdf
            rospy.logwarn("Executing home movement...")
            self.group.set_named_target("home")
            self.group.go(wait=True)
            rospy.logwarn("Executing -home- movement...DONE")

            rospy.logwarn("Executing -retract- movement...")
            self.group.set_named_target("retract")
            self.group.go(wait=True)

            self.group.set_named_target("vertical")
            self.group.go(wait=True)


def main():
    # Initialize the node
    rospy.init_node('test_kinovagen3_moveit')
    try:
        # Sleep before unpausing Gazebo so the robot does not fall
        time.sleep(5)

        # Unpause the physics
        # This will let MoveIt finish its initialization
        rospy.wait_for_service('/gazebo/unpause_physics')
        unpause_gazebo = rospy.ServiceProxy('/gazebo/unpause_physics', Empty)
        resp = unpause_gazebo()

        test_kinovagen3_obj = TestMoveitKinova()
        test_kinovagen3_obj.start_test()

    except KeyboardInterrupt:
        return

if __name__ == '__main__':
  main()
